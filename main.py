import requests
from bs4 import BeautifulSoup

# Setting User-Agent header for further GET request.
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:70.0) Gecko/20100101 Firefox/70.0'}
# Sending GET request to a time displaying website and storing the response.
response = requests.get('https://www.bahesab.ir/time', headers=headers)

# Using BeautifulSoup method to parse the HTML content of gotten response.
soup = BeautifulSoup(response.content, 'html.parser')
date_11_tag = soup.find(id='date-11')  # Finding and storing the tag containing the jalali date.
# Getting the text(date) of the tag and splitting it to [year,month,day].
date = date_11_tag.text.split(' / ')

# Converting the gotten date from persian number to regular number.
for i in range(len(date)):
    persian_character_num = date[i]
    num = ''
    for j in persian_character_num:
        num += str(ord(j) - 1776)
    date[i] = num

print('Date: {}/{:0>2}/{:0>2}'.format(date[2], date[1], date[0]))  # Printing date in proper format.
